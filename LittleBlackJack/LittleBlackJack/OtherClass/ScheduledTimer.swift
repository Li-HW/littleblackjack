//
//  ScheduledTimer.swift
//  WZPlatform
//
//  Created by 李浩彣 on 2020/1/7.
//  Copyright © 2020 101. All rights reserved.
//

import Foundation
//var timer : Timer?
//var countTimer = Int()
//let scheduledTimer = ScheduledTimer()
class ScheduledTimer: NSObject {
    var timer : Timer?
    var countTimer = Int()
    //    static let shard = ScheduledTimer()
    init(classTimeInterval : TimeInterval, classTarget : Any, classSelector : Selector, classUserInfo: Any?, classRepeats : Bool) {
        timer = Timer.scheduledTimer(timeInterval: classTimeInterval, target: classTarget, selector: classSelector, userInfo: nil, repeats: classRepeats)
        RunLoop.current.add(timer!, forMode: RunLoop.Mode.common)
    }
//    func createTimer(classTimeInterval : TimeInterval, classTarget : Any, classSelector : Selector, classUserInfo: Any?, classRepeats : Bool) {
//        timer = Timer.scheduledTimer(timeInterval: classTimeInterval, target: classTarget, selector: classSelector, userInfo: nil, repeats: classRepeats)
//        RunLoop.current.add(timer!, forMode: RunLoop.Mode.common)
//    }
    
    //    @objc func timeRepeat(_ time:Timer) -> Void {
    ////        HomePageView_B?.coinAnimation()
    //        print("timer Count:\(countTimer)")
    //        countTimer+=1
    //    }
    func timerIsRunning() -> Bool {
        if timer != nil {
            return timer!.isValid
        }
        return false
        
    }
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
}
