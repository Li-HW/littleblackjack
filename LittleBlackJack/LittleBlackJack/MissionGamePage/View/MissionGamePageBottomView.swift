//
//  MissionGamePageBottomView.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/5.
//  Copyright © 2020 Jack. All rights reserved.
//

import UIKit

class MissionGamePageBottomView: UIView {
    var manaSlider = UISlider()
    var customSliderBG = UIImageView()
    var customSliderFG = UIImageView()
    
    var fireElementBtn = UIButton()
    var iceElementBtn = UIButton()
    var windElementBtn = UIButton()
    var thunderElementBtn = UIButton()
    var lightElementBtn = UIButton()
    var darkElementBtn = UIButton()
    
    var fireElementUpBtn = UIButton()
    var iceElementUpBtn = UIButton()
    var windElementUpBtn = UIButton()
    var thunderElementUpBtn = UIButton()
    var lightElementUpBtn = UIButton()
    var darkElementUpBtn = UIButton()
    
    var moneyLbl = UILabel()
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUI() {

        
        let customSliderBG = UIImageView()
        customSliderBG.backgroundColor = .white
        self.addSubview(customSliderBG)
        customSliderBG.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.85)
            make.height.equalToSuperview().multipliedBy(0.2)
            make.bottom.equalToSuperview().multipliedBy(0.25)
        }
        self.customSliderBG = customSliderBG
        
        let customSliderFG = UIImageView()
        customSliderFG.backgroundColor = .black
        self.addSubview(customSliderFG)
        customSliderFG.snp.makeConstraints { (make) in
            make.centerY.left.equalTo(self.customSliderBG)
            make.width.equalTo(self.customSliderBG).multipliedBy(0.4)
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.customSliderFG = customSliderFG
        
        for i in 0..<6 {
            let bg = UIView()
            bg.tag = i + 1
            self.addSubview(bg)
            bg.snp.makeConstraints { (make) in
                if i == 0 {
                    make.left.equalTo(self.customSliderBG)
                }else if i == 5 {
                    make.left.equalTo(self.subviews[self.subviews.count - 2].snp.right)
                    make.right.equalTo(self.customSliderBG)
                    make.width.equalTo(self.subviews[self.subviews.count - 2])
                }else {
                    make.left.equalTo(self.subviews[self.subviews.count - 2].snp.right)
                    make.width.equalTo(self.subviews[self.subviews.count - 2])
                }
                make.height.equalToSuperview().multipliedBy(0.2)
                make.bottom.equalToSuperview().multipliedBy(0.5)
            }
        }
         
        let fireElementBtn = UIButton()
        fireElementBtn.tag = 1
        fireElementBtn.backgroundColor = .red
        fireElementBtn.setTitle("\(scenesModel.fireElementLv)", for: .normal)
        self.addSubview(fireElementBtn)
        fireElementBtn.snp.makeConstraints { (make) in
            make.center.equalTo(getBg(tag: 1)!)
            make.width.equalTo(fireElementBtn.snp.height)
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.fireElementBtn = fireElementBtn
        
        let fireElementUpBtn = UIButton()
        fireElementUpBtn.tag = 1
        fireElementUpBtn.backgroundColor = .red
        fireElementUpBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        fireElementUpBtn.setTitle("up", for: .normal)
        self.addSubview(fireElementUpBtn)
        fireElementUpBtn.snp.makeConstraints { (make) in
            make.centerX.width.equalTo(fireElementBtn)
            make.top.equalTo(fireElementBtn.snp.bottom).offset(5)
            make.height.equalTo(fireElementBtn).multipliedBy(0.5)
        }
        self.fireElementUpBtn = fireElementUpBtn
        
        let iceElementBtn = UIButton()
        iceElementBtn.tag = 2
        iceElementBtn.backgroundColor = .blue
        iceElementBtn.setTitle("\(scenesModel.iceElementLv)", for: .normal)
        self.addSubview(iceElementBtn)
        iceElementBtn.snp.makeConstraints { (make) in
            make.center.equalTo(getBg(tag: 2)!)
            make.width.equalTo(iceElementBtn.snp.height)
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.iceElementBtn = iceElementBtn
        
        let iceElementUpBtn = UIButton()
        iceElementUpBtn.tag = 2
        iceElementUpBtn.backgroundColor = .blue
        iceElementUpBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        iceElementUpBtn.setTitle("up", for: .normal)
        self.addSubview(iceElementUpBtn)
        iceElementUpBtn.snp.makeConstraints { (make) in
            make.centerX.width.equalTo(iceElementBtn)
            make.top.equalTo(iceElementBtn.snp.bottom).offset(5)
            make.height.equalTo(iceElementBtn).multipliedBy(0.5)
        }
        self.iceElementUpBtn = iceElementUpBtn
        
        let windElementBtn = UIButton()
        windElementBtn.tag = 3
        windElementBtn.backgroundColor = .green
        windElementBtn.setTitle("\(scenesModel.windElementLv)", for: .normal)
        self.addSubview(windElementBtn)
        windElementBtn.snp.makeConstraints { (make) in
            make.center.equalTo(getBg(tag: 3)!)
            make.width.equalTo(windElementBtn.snp.height)
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.windElementBtn = windElementBtn
        
        let windElementUpBtn = UIButton()
        windElementUpBtn.tag = 3
        windElementUpBtn.backgroundColor = .green
        windElementUpBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        windElementUpBtn.setTitle("up", for: .normal)
        self.addSubview(windElementUpBtn)
        windElementUpBtn.snp.makeConstraints { (make) in
            make.centerX.width.equalTo(windElementBtn)
            make.top.equalTo(windElementBtn.snp.bottom).offset(5)
            make.height.equalTo(windElementBtn).multipliedBy(0.5)
        }
        self.windElementUpBtn = windElementUpBtn

        let thunderElementBtn = UIButton()
        thunderElementBtn.tag = 4
        thunderElementBtn.backgroundColor = .yellow
        thunderElementBtn.setTitle("\(scenesModel.thunderElementLv)", for: .normal)
        self.addSubview(thunderElementBtn)
        thunderElementBtn.snp.makeConstraints { (make) in
            make.center.equalTo(getBg(tag: 4)!)
            make.width.equalTo(thunderElementBtn.snp.height)
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.thunderElementBtn = thunderElementBtn
        
        let thunderElementUpBtn = UIButton()
        thunderElementUpBtn.tag = 4
        thunderElementUpBtn.backgroundColor = .yellow
        thunderElementUpBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        thunderElementUpBtn.setTitle("up", for: .normal)
        self.addSubview(thunderElementUpBtn)
        thunderElementUpBtn.snp.makeConstraints { (make) in
            make.centerX.width.equalTo(thunderElementBtn)
            make.top.equalTo(thunderElementBtn.snp.bottom).offset(5)
            make.height.equalTo(thunderElementBtn).multipliedBy(0.5)
        }
        self.thunderElementUpBtn = thunderElementUpBtn
        
        let lightElementBtn = UIButton()
        lightElementBtn.tag = 5
        lightElementBtn.backgroundColor = .lightGray
        lightElementBtn.setTitle("\(scenesModel.lightElementLv)", for: .normal)
        self.addSubview(lightElementBtn)
        lightElementBtn.snp.makeConstraints { (make) in
            make.center.equalTo(getBg(tag: 5)!)
            make.width.equalTo(lightElementBtn.snp.height)
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.lightElementBtn = lightElementBtn
        
        let lightElementUpBtn = UIButton()
        lightElementUpBtn.tag = 5
        lightElementUpBtn.backgroundColor = .lightGray
        lightElementUpBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        lightElementUpBtn.setTitle("up", for: .normal)
        self.addSubview(lightElementUpBtn)
        lightElementUpBtn.snp.makeConstraints { (make) in
            make.centerX.width.equalTo(lightElementBtn)
            make.top.equalTo(lightElementBtn.snp.bottom).offset(5)
            make.height.equalTo(lightElementBtn).multipliedBy(0.5)
        }
        self.lightElementUpBtn = lightElementUpBtn
        
        let darkElementBtn = UIButton()
        darkElementBtn.tag = 6
        darkElementBtn.backgroundColor = .black
        darkElementBtn.setTitle("\(scenesModel.darkElementLv)", for: .normal)
        self.addSubview(darkElementBtn)
        darkElementBtn.snp.makeConstraints { (make) in
            make.center.equalTo(getBg(tag: 6)!)
            make.width.equalTo(darkElementBtn.snp.height)
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.darkElementBtn = darkElementBtn
        
        let darkElementUpBtn = UIButton()
        darkElementUpBtn.tag = 6
        darkElementUpBtn.backgroundColor = .black
        darkElementUpBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        darkElementUpBtn.setTitle("up", for: .normal)
        self.addSubview(darkElementUpBtn)
        darkElementUpBtn.snp.makeConstraints { (make) in
            make.centerX.width.equalTo(darkElementBtn)
            make.top.equalTo(darkElementBtn.snp.bottom).offset(5)
            make.height.equalTo(darkElementBtn).multipliedBy(0.5)
        }
        self.darkElementUpBtn = darkElementUpBtn
        
        let moneyLbl = UILabel()
        moneyLbl.text = "\(scenesModel.money)"
        self.addSubview(moneyLbl)
        moneyLbl.snp.makeConstraints { (make) in
            make.height.equalTo(fireElementBtn).multipliedBy(0.5)
            make.top.equalTo(fireElementUpBtn.snp.bottom).offset(5)
            make.left.equalTo(getBg(tag:1)!)
        }
        self.moneyLbl = moneyLbl
    }
    
    func getBg(tag: Int) -> UIView? {
        for i in 0..<self.subviews.count {
            if self.subviews[i].tag == tag {
                return self.subviews[i]
            }
        }
        return nil
    }
    
    
    
    
    
}

