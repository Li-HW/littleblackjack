//
//  MissionGamePageMiddleView.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/5.
//  Copyright © 2020 Jack. All rights reserved.
//

import UIKit

class MissionGamePageMiddleView: UIView {
    var checkerboardBG: UIView!
    var settlementBtn: UIButton!
    var elementArr: [UIImageView?] = [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
    let mosterPath = UIBezierPath()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUI() {
        let checkerboardBG = UIView()
        checkerboardBG.backgroundColor = .gray
        self.addSubview(checkerboardBG)
        checkerboardBG.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.55)
            make.height.equalTo(checkerboardBG.snp.width)
        }
        self.checkerboardBG = checkerboardBG
        addCheckerboardBtn(bg: checkerboardBG)
        
        let settlementBtn = UIButton()
        settlementBtn.backgroundColor = .black
        self.addSubview(settlementBtn)
        settlementBtn.snp.makeConstraints { (make) in
            make.centerY.right.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.06)
            make.height.equalTo(settlementBtn.snp.width).multipliedBy(2)
        }
        self.settlementBtn = settlementBtn
    }
    override func layoutSubviews() {
        super.layoutSubviews()
//        let path = UIBezierPath()
        mosterPath.move(to: CGPoint(x: 0, y: self.checkerboardBG.frame.origin.y + self.checkerboardBG.frame.size.height / 2))
        mosterPath.addLine(to: CGPoint(x: self.checkerboardBG.frame.origin.x / 2, y: self.checkerboardBG.frame.origin.y + self.checkerboardBG.frame.size.height / 2))
        mosterPath.addLine(to: CGPoint(x: self.checkerboardBG.frame.origin.x / 2, y: self.checkerboardBG.frame.origin.y / 2))
        mosterPath.addLine(to: CGPoint(x: self.frame.size.width - ((self.frame.size.width - (self.checkerboardBG.frame.origin.x + self.checkerboardBG.frame.size.width)) / 2), y: self.checkerboardBG.frame.origin.y / 2))
        mosterPath.addLine(to: CGPoint(x: self.frame.size.width - ((self.frame.size.width - (self.checkerboardBG.frame.origin.x + self.checkerboardBG.frame.size.width)) / 2), y: self.frame.size.height  - ((self.frame.size.height - (self.checkerboardBG.frame.origin.y + self.checkerboardBG.frame.size.height)) / 2)))
        mosterPath.addLine(to: CGPoint(x: self.checkerboardBG.frame.origin.x / 2, y: self.frame.size.height  - ((self.frame.size.height - (self.checkerboardBG.frame.origin.y + self.checkerboardBG.frame.size.height)) / 2)))
        mosterPath.addLine(to: CGPoint(x: self.checkerboardBG.frame.origin.x / 2, y: self.checkerboardBG.frame.origin.y + self.checkerboardBG.frame.size.height / 2))
        mosterPath.close()
        let triangleLayer = CAShapeLayer()
        triangleLayer.path = mosterPath.cgPath
        triangleLayer.fillColor = UIColor.clear.cgColor
        triangleLayer.strokeColor = UIColor.black.cgColor
        triangleLayer.lineWidth = 4
        self.layer.addSublayer(triangleLayer)
    }
    
    func addCheckerboardBtn(bg: UIView) {
        for i in 0..<25 {
            let view = UIView()
            view.tag = i + 1
//            btn.setTitle("\(i)", for: .normal)
//            btn.backgroundColor = i % 2 == 0 ? UIColor.orange : UIColor.black
            view.layer.borderWidth = 0.5
            bg.addSubview(view)
//            let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
//            btn.addGestureRecognizer(gestureRecognizer)
            
            view.snp.makeConstraints { (make) in
                make.width.equalToSuperview().multipliedBy(0.2)
                make.height.equalTo(view.snp.width)
                if i == 0 {
                    make.top.left.equalToSuperview()
                }else {
                    if i % 5 == 0 {
                        make.top.equalTo(bg.subviews[i-1].snp.bottom)
                        make.left.equalToSuperview()
                    }else {
                        make.top.equalTo(bg.subviews[i-1])
                        make.left.equalTo(bg.subviews[i-1].snp.right)
                    }
                }
            }
        }
    }

}
