//
//  MissionGamePageTopView.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/5.
//  Copyright © 2020 Jack. All rights reserved.
//

import UIKit

class MissionGamePageTopView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUI() {
        let trashBin = UIImageView()
        trashBin.backgroundColor = .green
        self.addSubview(trashBin)
        trashBin.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.1)
            make.height.equalTo(trashBin.snp.width)
            make.bottom.right.equalToSuperview()
        }
    }
}
