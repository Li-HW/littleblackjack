//
//  MissionGamePageVC.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/5.
//  Copyright © 2020 Jack. All rights reserved.
//

import UIKit

class MissionGamePageVC: UIViewController {
    var top = MissionGamePageTopView()
    var middle = MissionGamePageMiddleView()
    var bottom = MissionGamePageBottomView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .green
        
        let top = MissionGamePageTopView()
        top.backgroundColor = .red
        view.addSubview(top)
        top.snp.makeConstraints { (make) in
            make.centerY.top.left.right.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.top = top
        
        let bottom = MissionGamePageBottomView()
        bottom.backgroundColor = .gray
        view.addSubview(bottom)
        bottom.snp.makeConstraints { (make) in
            make.centerY.bottom.left.right.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.2)
        }
        self.bottom = bottom
        bottom.fireElementBtn.addTarget(self, action: #selector(summonElementBtn), for: .touchUpInside)
        bottom.iceElementBtn.addTarget(self, action: #selector(summonElementBtn), for: .touchUpInside)
        bottom.windElementBtn.addTarget(self, action: #selector(summonElementBtn), for: .touchUpInside)
        bottom.thunderElementBtn.addTarget(self, action: #selector(summonElementBtn), for: .touchUpInside)
        bottom.lightElementBtn.addTarget(self, action: #selector(summonElementBtn), for: .touchUpInside)
        bottom.darkElementBtn.addTarget(self, action: #selector(summonElementBtn), for: .touchUpInside)
        
        bottom.fireElementUpBtn.addTarget(self, action: #selector(elementUpBtn), for: .touchUpInside)
        bottom.iceElementUpBtn.addTarget(self, action: #selector(elementUpBtn), for: .touchUpInside)
        bottom.windElementUpBtn.addTarget(self, action: #selector(elementUpBtn), for: .touchUpInside)
        bottom.thunderElementUpBtn.addTarget(self, action: #selector(elementUpBtn), for: .touchUpInside)
        bottom.lightElementUpBtn.addTarget(self, action: #selector(elementUpBtn), for: .touchUpInside)
        bottom.darkElementUpBtn.addTarget(self, action: #selector(elementUpBtn), for: .touchUpInside)
        
        let middle = MissionGamePageMiddleView()
        middle.backgroundColor = .white
        view.addSubview(middle)
        middle.snp.makeConstraints { (make) in
            make.centerY.left.right.equalToSuperview()
            make.top.equalTo(top.snp.bottom)
            make.bottom.equalTo(bottom.snp.top)
        }
        self.middle = middle
        middle.settlementBtn.addTarget(self, action: #selector(settlement), for: .touchUpInside)
// MARK:怪物&元素Model建構
        /*
        let a = UIImageView()
        a.monsterModel = MonsterModel()
        a.monsterModel?.hp = 100
        a.monsterModel?.debuffStatus.burn = true
        a.monsterModel?.immunityStatus.burn = true
        print(a.monsterModel)

        a.elementModel = ElementModel()
        a.elementModel?.buffStatus.attUp = true
        print(a.elementModel)
         */
        
// MARK:Model To Dictionary兩個方式皆可
        /*
        let aa = try? a.monsterModel?.debuffStatus.asDictionary()
        let aaa = a.monsterModel?.debuffStatus.dictionary
        */
//        let test = UIImageView()
//        test.elementModel = ElementModel()
//        test.elementModel?.elementType = .fire
//
//        let test2 = UIImageView()
//        test2.elementModel = ElementModel()
//        test2.elementModel?.elementType = .fire
// MARK:元素合併
//        let a = elementMerge(element1: test, element2: test2)
//        self.view.addSubview(a)
// MARK:能量條Timer
        _ = ScheduledTimer.init(classTimeInterval: 0.1, classTarget: self, classSelector: #selector(manaSliderTime), classUserInfo: nil, classRepeats: true)
// MARK:怪物克隆
        delay(seconds: 1) {
            let monsterArr = self.monsterClone(monsterSizeEm: .small, monsterPoint: CGPoint(x: 0, y: 0))
            // Do any additional setup after loading the view.
            

            for i in 0...(monsterArr.count - 1) {
                let pathToLeftAnimation = CAKeyframeAnimation(keyPath: "position\(i)")
                pathToLeftAnimation.path = self.middle.mosterPath.cgPath
                pathToLeftAnimation.calculationMode = CAAnimationCalculationMode.paced
                pathToLeftAnimation.duration = 1.5
                
                let delayGroupAnimation = CAAnimationGroup()
                delayGroupAnimation.animations = [pathToLeftAnimation]
                delayGroupAnimation.duration = 1.5
                let tempMonster = monsterArr[i]
                tempMonster.layer.add(delayGroupAnimation, forKey: nil)
                tempMonster.isHidden = false
                self.middle.addSubview(tempMonster)
                sleep(1)
            }
        }
        
    }
    
// MARK:元素賣出
    func elementSell(element: UIImageView) {
        let rankLv = element.elementModel!.elementType!.rawValue.split(separator: "/")
        var elementPrice = Int(Double((rankLv.count) * scenesModel.elementBase) * scenesModel.rankProportion)
        if element.elementModel!.elementLv == 1 {
            scenesModel.money = scenesModel.money + Int(ceil(Double(elementPrice) * scenesModel.sellProportion))
        }else {
            for _ in 1..<element.elementModel!.elementLv {
                elementPrice = Int(Double(elementPrice) * 2.2)
            }
            scenesModel.money = scenesModel.money + Int(ceil(Double(elementPrice) * scenesModel.sellProportion))
        }
        self.bottom.moneyLbl.text = "\(scenesModel.money)"
        element.removeFromSuperview()
    }
    
// MARK:結算
    @objc func settlement() {
        var settlementArr: [Int] = []
        for i in 0..<middle.elementArr.count {
            if middle.elementArr[i] != nil {
                let rankLv = middle.elementArr[i]?.elementModel!.elementType!.rawValue.split(separator: "/")
                var elementPrice = Int(Double((rankLv!.count) * scenesModel.elementBase) * scenesModel.rankProportion)
                if middle.elementArr[i]!.elementModel!.elementLv == 1 {
                    settlementArr.append(elementPrice)
                }else {
                    for _ in 1..<middle.elementArr[i]!.elementModel!.elementLv {
                        elementPrice = Int(Double(elementPrice) * 2.2)
                    }
                    settlementArr.append(elementPrice)
                }
            }
        }
// MARK:元素結算
        var total: Int = 0
        for i in 0..<settlementArr.count {
            total = total + Int(ceil(Double(settlementArr[i]) * scenesModel.settlementProportion))
        }
// MARK:場景結算
        let scenesMoney = Int(ceil(Double(scenesModel.money) * scenesModel.settlementProportion))
        print(total + scenesMoney)
    }
    
// MARK:能量條
    @objc func manaSliderTime() {
        
        let totalWidth = bottom.customSliderBG.frame.size.width
        let nowWidth = bottom.customSliderFG.frame.size.width
        var newWidth = nowWidth + (totalWidth*(scenesModel.mana/100))
        if newWidth > totalWidth {
            newWidth = totalWidth
        }
        UIView.animate(withDuration: 0.09) {
            self.bottom.customSliderFG.snp.remakeConstraints { (make) in
                make.centerY.left.equalTo(self.bottom.customSliderBG)
                make.width.equalTo(newWidth)
                make.height.equalToSuperview().multipliedBy(0.2)
            }
            self.bottom.layoutIfNeeded()
        }
    }
    
// MARK:扣能量&招喚元素
    @objc func summonElementBtn(sender: UIButton) {
        
        let summonLV = (sender.titleLabel!.text! as NSString).floatValue
        let totalWidth = bottom.customSliderBG.frame.size.width
        let nowWidth = bottom.customSliderFG.frame.size.width
        var newWidth: CGFloat!
        if nowWidth >= (totalWidth * CGFloat((0.1 * summonLV))), summonElement(sender: sender) {
            newWidth = nowWidth - (totalWidth*CGFloat((0.1 * summonLV)))
            UIView.animate(withDuration: 0.09) {
                self.bottom.customSliderFG.snp.remakeConstraints { (make) in
                    make.centerY.left.equalTo(self.bottom.customSliderBG)
                    make.width.equalTo(newWidth)
                    make.height.equalToSuperview().multipliedBy(0.2)
                }
                self.bottom.layoutIfNeeded()
            }
        }
    }
// MARK:招喚元素
    func summonElement(sender: UIButton) -> Bool {
        let arr = checkElementArr()
        if arr.count == 0 {
            return false
        }
        let random = Int.random(in: 0..<arr.count)
        for i in 0..<self.middle.checkerboardBG.subviews.count {
            if i == arr[random] {
                let newImgView = UIImageView()
                newImgView.isUserInteractionEnabled = true
                newImgView.frame = self.middle.checkerboardBG.subviews[i].frame
                newImgView.elementModel = ElementModel()
                newImgView.tag = i + 1
                if sender.tag == 1 {
                    newImgView.elementModel?.elementType = .fire
                    newImgView.elementModel?.elementLv = scenesModel.fireElementLv
                    newImgView.backgroundColor = .red
                }
                if sender.tag == 2 {
                    newImgView.elementModel?.elementType = .ice
                    newImgView.elementModel?.elementLv = scenesModel.iceElementLv
                    newImgView.backgroundColor = .blue
                }
                if sender.tag == 3 {
                    newImgView.elementModel?.elementType = .wind
                    newImgView.elementModel?.elementLv = scenesModel.windElementLv
                    newImgView.backgroundColor = .green
                }
                if sender.tag == 4 {
                    newImgView.elementModel?.elementType = .thunder
                    newImgView.elementModel?.elementLv = scenesModel.thunderElementLv
                    newImgView.backgroundColor = .yellow
                }
                if sender.tag == 5 {
                    newImgView.elementModel?.elementType = .light
                    newImgView.elementModel?.elementLv = scenesModel.lightElementLv
                    newImgView.backgroundColor = .lightGray
                }
                if sender.tag == 6 {
                    newImgView.elementModel?.elementType = .dark
                    newImgView.elementModel?.elementLv = scenesModel.darkElementLv
                    newImgView.backgroundColor = .black
                }
                getElementStatusfromElementTypeEm(element: newImgView)
                self.middle.checkerboardBG.addSubview(newImgView)
                let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
                newImgView.addGestureRecognizer(gestureRecognizer)
                middle.elementArr[i] = newImgView
            }
        }
        
        return true
    }
    
// MARK:元素移動
    @objc func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            let translation = gestureRecognizer.translation(in: self.view)
            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        if gestureRecognizer.state == .ended {
            var isOver = true
            let oldImageView:UIImageView
            var tempTag:Int = -1
            for i in 0...(self.middle.checkerboardBG.subviews.count - 1) {
                var tempView = self.middle.checkerboardBG.subviews[i]
                if tempView is UIView && !(tempView is UIImageView) {
                    print("handlePantest .\(i)")
                    if ((gestureRecognizer.view!.center.x >= tempView.frame.origin.x)&&(gestureRecognizer.view!.center.x <= (tempView.frame.origin.x+tempView.frame.size.width))&&(gestureRecognizer.view!.center.y >= tempView.frame.origin.y)&&(gestureRecognizer.view!.center.y <= (tempView.frame.origin.y+tempView.frame.size.height))){
                        print("get handlePantest .\(i)")
                        tempTag = i + 1
                        
                        
                    }
                }
                if tempView is UIImageView {
                    let tempView1 = tempView as! UIImageView
                    let tempView2 = gestureRecognizer.view as! UIImageView
                    
                    if (tempView.tag == tempTag) && ((tempView1.elementModel?.elementLv) == tempView2.elementModel?.elementLv){
//                        oldImageView = tempView
                        isOver = false
                        gestureRecognizer.view!.center = CGPoint(x: tempView.center.x, y: tempView.center.y)
                        gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
                        self.middle.checkerboardBG.addSubview(elementMerge(element1: tempView as! UIImageView, element2: gestureRecognizer.view as! UIImageView))
                        break
                    }
                }
            }
            if isOver {
                let tempView = self.middle.checkerboardBG.subviews[gestureRecognizer.view!.tag-1]
                gestureRecognizer.view!.center = CGPoint(x: tempView.center.x, y: tempView.center.y)
                gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
            }
//            let translation = gestureRecognizer.translation(in: self.view)
//            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
//            gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
    }
    
// MARK:整理元素版面還剩下空間
    func checkElementArr() -> [Int] {
        var intArr: [Int] = []
        for i in 0..<middle.elementArr.count {
            if middle.elementArr[i] == nil {
                intArr.append(i)
            }
        }
        return intArr
    }
    
// MARK:元素升級&扣金額
    @objc func elementUpBtn(sender: UIButton) {
        let pay = [100, 200, 300, 500, 800, 1300, 2100, 3400, 5500]
        if sender.tag == 1, scenesModel.fireElementLv != 10, scenesModel.money >= pay[scenesModel.fireElementLv - 1] {
            scenesModel.money = scenesModel.money - pay[scenesModel.fireElementLv - 1]
            scenesModel.fireElementLv = scenesModel.fireElementLv + 1
            self.bottom.fireElementBtn.setTitle("\(scenesModel.fireElementLv)", for: .normal)
            self.bottom.moneyLbl.text = "\(scenesModel.money)"
        }
        if sender.tag == 2, scenesModel.iceElementLv != 10, scenesModel.money >= pay[scenesModel.iceElementLv - 1] {
            scenesModel.money = scenesModel.money - pay[scenesModel.iceElementLv - 1]
            scenesModel.iceElementLv = scenesModel.iceElementLv + 1
            self.bottom.iceElementBtn.setTitle("\(scenesModel.iceElementLv)", for: .normal)
            self.bottom.moneyLbl.text = "\(scenesModel.money)"
        }
        if sender.tag == 3, scenesModel.windElementLv != 10, scenesModel.money >= pay[scenesModel.windElementLv - 1] {
            scenesModel.money = scenesModel.money - pay[scenesModel.windElementLv - 1]
            scenesModel.windElementLv = scenesModel.windElementLv + 1
            self.bottom.windElementBtn.setTitle("\(scenesModel.windElementLv)", for: .normal)
            self.bottom.moneyLbl.text = "\(scenesModel.money)"
        }
        if sender.tag == 4, scenesModel.thunderElementLv != 10, scenesModel.money >= pay[scenesModel.thunderElementLv - 1] {
            scenesModel.money = scenesModel.money - pay[scenesModel.thunderElementLv - 1]
            scenesModel.thunderElementLv = scenesModel.thunderElementLv + 1
            self.bottom.thunderElementBtn.setTitle("\(scenesModel.thunderElementLv)", for: .normal)
            self.bottom.moneyLbl.text = "\(scenesModel.money)"
        }
        if sender.tag == 5, scenesModel.lightElementLv != 10, scenesModel.money >= pay[scenesModel.lightElementLv - 1] {
            scenesModel.money = scenesModel.money - pay[scenesModel.lightElementLv - 1]
            scenesModel.lightElementLv = scenesModel.lightElementLv + 1
            self.bottom.lightElementBtn.setTitle("\(scenesModel.lightElementLv)", for: .normal)
            self.bottom.moneyLbl.text = "\(scenesModel.money)"
        }
        if sender.tag == 6, scenesModel.darkElementLv != 10, scenesModel.money >= pay[scenesModel.darkElementLv - 1] {
            scenesModel.money = scenesModel.money - pay[scenesModel.darkElementLv - 1]
            scenesModel.darkElementLv = scenesModel.darkElementLv + 1
            self.bottom.darkElementBtn.setTitle("\(scenesModel.darkElementLv)", for: .normal)
            self.bottom.moneyLbl.text = "\(scenesModel.money)"
        }
    }

// MARK:設定怪物SizeEm跟關卡回傳血量陣列
    func monsterHP(monsterSizeEm: MonsterSizeEm, stage: Int) -> [Int]? {
        var monsterArr = [Int]()
        let count = stage / 5 + stage / 10
        let hpMultipliedBy = stage * count * monsterHPModel.basicHpMultipliedBy
        let lowHp = (monsterHPModel.basicHp + hpMultipliedBy) * stage
        let highHp = (monsterHPModel.basicHp + hpMultipliedBy) * (stage + 1)
        if monsterSizeEm == .small {
            for i in 1...20 {
                let mosterHp = Int(Float(highHp - lowHp)/Float(20.0) * Float(i) + Float(lowHp))
                monsterArr.append(mosterHp)
            }
            return monsterArr
        }
        if monsterSizeEm == .medium {
            let mosterHp = highHp * monsterHPModel.smallBossHpMultipliedBy
            monsterArr.append(mosterHp)
            return monsterArr
        }
        if monsterSizeEm == .large {
            let mosterHp = highHp * monsterHPModel.bigBossHpMultipliedBy
            monsterArr.append(mosterHp)
            return monsterArr
        }
        return nil
    }

// MARK:怪物克隆初始&血量設定 需設定初始CGPoint 怪物Size為內容預設 回傳隱藏的怪物陣列
    func monsterClone(monsterSizeEm: MonsterSizeEm, monsterPoint: CGPoint) -> [UIImageView] {
        var monsterSize = CGSize()
        if monsterSizeEm == .small {
            monsterSize = CGSize(width: 50, height: 50)
        }
        if monsterSizeEm == .medium {
            monsterSize = CGSize(width: 75, height: 75)
        }
        if monsterSizeEm == .large {
            monsterSize = CGSize(width: 100, height: 100)
        }
        let monsterRect = CGRect(origin: monsterPoint, size: monsterSize)
        let monsterHpArr = monsterHP(monsterSizeEm: monsterSizeEm, stage: scenesModel.stage)!
        var monsterArr = [UIImageView]()
        for i in 0..<monsterHpArr.count {
            let monster = UIImageView()
            monster.backgroundColor = i % 2 == 0 ? UIColor.orange : UIColor.black
            monster.frame = monsterRect
            monster.monsterModel = MonsterModel()
            monster.monsterModel?.hp = monsterHpArr[i]
            monster.isHidden = true
            self.middle.addSubview(monster)
            
            let monsterHpLbl = UILabel()
//            monsterHpLbl.frame = position
            monsterHpLbl.textAlignment = .center
            monsterHpLbl.text = "\(monster.monsterModel!.hp!)"
            monster.addSubview(monsterHpLbl)
            monsterHpLbl.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            monsterArr.append(monster)
        }
        return monsterArr
        
        
    }

// MARK:元素合併
    func elementMerge(element1: UIImageView, element2: UIImageView) -> UIImageView {
        
        let elementTypeEmStr = getElementTypeEmStr(element1Str: element1.elementModel!.elementType!.rawValue, element2Str: element2.elementModel!.elementType!.rawValue)
        let newImgView = UIImageView()
        newImgView.tag = element1.tag
        newImgView.isUserInteractionEnabled = true
        newImgView.frame = element1.frame
        newImgView.elementModel = ElementModel()
        newImgView.elementModel?.elementType = ElementTypeEm(rawValue: elementTypeEmStr)!
        getElementStatusfromElementTypeEm(element: newImgView)

        newImgView.backgroundColor = .systemPurple

        newImgView.elementModel?.elementLv = getElementLv(element1: element1, element2: element2)
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        newImgView.addGestureRecognizer(gestureRecognizer)
        middle.elementArr[element1.tag - 1] = newImgView
        middle.elementArr[element2.tag - 1] = nil
        element1.removeFromSuperview()
        element2.removeFromSuperview()

        return newImgView
    }
    
// MARK:新元素類別獲取
    func getElementTypeEmStr(element1Str: String, element2Str: String) -> String {
        var newStr = String()
        let arr1 = element1Str.split(separator: "/")
        let arr2 = element2Str.split(separator: "/")
        if arr1.count == 1, arr2.count == 1 {
            newStr = elementSort(arr1: arr1, arr2: arr2)
        }else {
            if element1Str == element2Str {
                newStr = element1Str
            }else {
                newStr = elementSort(arr1: arr1, arr2: arr2)
            }
        }
        return newStr
    }
// MARK:新元素等級獲取
    func getElementLv(element1: UIImageView, element2: UIImageView) -> Int {
        var newLv = 1
        let arr1 = element1.elementModel!.elementType!.rawValue.split(separator: "/")
        let arr2 = element2.elementModel!.elementType!.rawValue.split(separator: "/")
        if arr1.count == 1, arr2.count == 1 {
            newLv = element1.elementModel!.elementLv
        }else {
            if element1.elementModel!.elementType!.rawValue == element2.elementModel!.elementType!.rawValue {
                newLv = element1.elementModel!.elementLv + 1
            }else {
                newLv = element1.elementModel!.elementLv
            }
        }
        return newLv
    }
    
// MARK:元素類別字串排序
    func elementSort(arr1: [String.SubSequence], arr2: [String.SubSequence]) -> String {
        var sortArr: [String] = []
        var sortStr = ""
        let checkList = ["fire", "ice", "wind", "thunder", "light", "dark"]
        for count in 0..<checkList.count {
            for i in 0..<arr1.count {
                if arr1[i] == checkList[count] {
                    sortArr.append(String(arr1[i]))
                }
            }
            for i in 0..<arr2.count {
                if arr2[i] == checkList[count] {
                    sortArr.append(String(arr2[i]))
                }
            }
        }
        for i in 0..<sortArr.count {
            if i == 0 {
                sortStr = sortStr + sortArr[i]
            }else {
                sortStr = sortStr + "/\(sortArr[i])"
            }
        }
        return sortStr
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

