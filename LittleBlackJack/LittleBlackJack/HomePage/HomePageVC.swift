//
//  HomePageVC.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/5.
//  Copyright © 2020 Jack. All rights reserved.
//

import UIKit
import SnapKit

class HomePageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let missionGameBtn = UIButton()
        missionGameBtn.backgroundColor = .red
        missionGameBtn.setTitle("mission Game", for: .normal)
        missionGameBtn.addTarget(self, action: #selector(pushToMissionGamePageVC), for: .touchUpInside)
        view.addSubview(missionGameBtn)
        missionGameBtn.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.4)
            make.height.equalTo(missionGameBtn.snp.width).multipliedBy(0.5)
        }
        // Do any additional setup after loading the view.
    }
    @objc func pushToMissionGamePageVC() {
        navigationController?.pushViewController(MissionGamePageVC(), animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
