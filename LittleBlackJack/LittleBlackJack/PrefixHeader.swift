//
//  PrefixHeader.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/9.
//  Copyright © 2020 Jack. All rights reserved.
//

import Foundation
import UIKit
// MARK:執行延遲
func delay(seconds: Double, completed:@escaping () -> ()){
    let deadlineTime = DispatchTime.now() + seconds
    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
        completed()
    }
}

func getElementStatusfromElementTypeEm(element: UIImageView) {
    let dic = getElementStatusDic(elementTypeEm: (element.elementModel?.elementType)!)
    let encoder = JSONEncoder()
    if let jsonData = try? encoder.encode(dic) {
        if let jsonString = String(data: jsonData, encoding: .utf8) {
            print(jsonString)
            let data = Data(jsonString.utf8)
            do{
                element.elementModel?.elementStatus = try JSONDecoder().decode(ElementStatus_ElementModel.self, from: data)
                element.elementModel?.elementStatus.attSort = AttSortEm(rawValue: (element.elementModel?.elementStatus.attSortStr)!)
            }catch {
                print("ElementStatus_ElementModel=Error")
            }
        }
    }
}


