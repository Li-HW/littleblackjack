//
//  UIImageView+Extension .swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/9.
//  Copyright © 2020 Jack. All rights reserved.
//

import Foundation
import UIKit

var key1: Void?
var key2: Void?
extension UIImageView {
// MARK:怪物&元素Model建構
    var monsterModel: MonsterModel? {
            get {
                return objc_getAssociatedObject(self, &key1) as? MonsterModel
            }

            set {
                objc_setAssociatedObject(self,&key1, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
    }
    var elementModel: ElementModel? {
            get {
                return objc_getAssociatedObject(self, &key2) as? ElementModel
            }

            set {
                objc_setAssociatedObject(self,&key2, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
    }
}
