//
//  Encodable+Extension.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/9.
//  Copyright © 2020 Jack. All rights reserved.
//

import Foundation
extension Encodable {
// MARK:Model To Dictionary
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
