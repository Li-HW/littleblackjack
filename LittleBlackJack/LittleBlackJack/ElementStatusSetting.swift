//
//  ElementStatusSetting.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/12.
//  Copyright © 2020 Jack. All rights reserved.
//

import Foundation

func getElementStatusDic(elementTypeEm: ElementTypeEm) -> [String:String] {
    switch elementTypeEm {
    // MARK:Rank1
    case .fire:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .ice:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .wind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .light:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .dark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    // MARK:Rank2
    case .fireFire:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireIce:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceIce:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .lightLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .lightDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .darkDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    // MARK:Rank3
    case .fireFireFire:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireFireIce:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireFireWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireFireThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireFireLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireFireDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireIceIce:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireIceWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireIceThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireIceLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireIceDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireWindWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireWindThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireWindLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireWindDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireThunderThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireThunderLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireThunderDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireLightLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireLightDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .fireDarkDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceIceIce:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceIceWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceIceThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceIceLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceIceDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceWindWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceWindThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceWindLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceWindDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceThunderThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceThunderLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceThunderDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceLightLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceLightDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .iceDarkDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windWindWind:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windWindThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windWindLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windWindDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windThunderThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windThunderLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windThunderDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windLightLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windLightDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .windDarkDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderThunderThunder:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderThunderLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderThunderDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderLightLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderLightDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .thunderDarkDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .lightLightLight:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .lightLightDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .lightDarkDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    case .darkDarkDark:
        return ["attSortStr": "first", "att": "1", "outAtt": "0", "inAtt": "0", "attSpeed": "0", "outAttSpeed": "0", "inAttSpeed": "0"]
    }
}
