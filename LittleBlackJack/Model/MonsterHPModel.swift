//
//  MonsterHPModel.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/8.
//  Copyright © 2020 Jack. All rights reserved.
//

//func <#func name#>(<#Incoming parameters#> completion:@escaping (_ dateResponse: DataResponse<Any>?)->()){
//    let parameters = ["<#Key#>": <#Value#>]
//    NetworkCenter.shard.postApiResponseWithBaseUrl(urlPath: <#Path Name#>, parameters: <#parameters#>) { (dateResponse) in
//        //            print("Api_login statusCode=\(response.response?.statusCode)")
//        guard let _ = dateResponse.response else {
//            //網路斷線時
//            completion(nil)
//            return
//        }
//        completion(dateResponse)
//    }
//}
enum MonsterSizeEm {
    case small, medium, large
}

import Foundation
var monsterHPModel: MonsterHPModel = MonsterHPModel()

struct MonsterHPModel: Codable {
    let basicHp = 200
    let basicHpMultipliedBy = 10
    let smallBossHpMultipliedBy = 5
    let bigBossHpMultipliedBy = 10
    
    enum CodingKeys: String, CodingKey {
        case basicHp, basicHpMultipliedBy, smallBossHpMultipliedBy, bigBossHpMultipliedBy
        /* 基礎血量, 加成血量基數, 小王血量倍數, 大王血量倍數 */
    }
}
