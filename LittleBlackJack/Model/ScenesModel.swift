//
//  ScenesModel.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/9.
//  Copyright © 2020 Jack. All rights reserved.
//

import Foundation
import UIKit
var scenesModel: ScenesModel = ScenesModel()

struct ScenesModel: Codable {
    var money = 85200
    var stage = 1
    var time = 0
    var mana: CGFloat = 10
// MARK:元素賣出&結算係數
    let elementBase = 5
    let sellProportion = 0.1
    let settlementProportion = 0.3
    let rankProportion = 0.3
// MARK:元素等級
    var fireElementLv = 1
    var iceElementLv = 1
    var windElementLv = 1
    var thunderElementLv = 1
    var lightElementLv = 1
    var darkElementLv = 1
    
    enum CodingKeys: String, CodingKey {
        case money, stage, time, mana, fireElementLv, iceElementLv, windElementLv, thunderElementLv, lightElementLv, darkElementLv
        /* 金錢, 關卡, 時間限制, 能量回復, 元素基底, 出售比例, 結算比例, Rank比例, 火元素等級, 冰元素等級, 風元素等級, 雷元素等級, 光元素等級, 暗元素等級 */
    }
}
