//
//  ElementModel.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/8.
//  Copyright © 2020 Jack. All rights reserved.
//

//func <#func name#>(<#Incoming parameters#> completion:@escaping (_ dateResponse: DataResponse<Any>?)->()){
//    let parameters = ["<#Key#>": <#Value#>]
//    NetworkCenter.shard.postApiResponseWithBaseUrl(urlPath: <#Path Name#>, parameters: <#parameters#>) { (dateResponse) in
//        //            print("Api_login statusCode=\(response.response?.statusCode)")
//        guard let _ = dateResponse.response else {
//            //網路斷線時
//            completion(nil)
//            return
//        }
//        completion(dateResponse)
//    }
//}
enum ElementTypeEm: String {
    // MARK:Rank1
    case fire, ice, wind, thunder, light, dark
    // MARK:Rank2
    case fireFire = "fire/fire", fireIce = "fire/ice", fireWind = "fire/wind", fireThunder = "fire/thunder", fireLight = "fire/light", fireDark = "fire/dark"
    case iceIce = "ice/ice", iceWind = "ice/wind", iceThunder = "ice/thunder", iceLight = "ice/light", iceDark = "ice/dark"
    case windWind = "wind/wind", windThunder = "wind/thunder", windLight = "wind/light", windDark = "wind/dark"
    case thunderThunder = "thunder/thunder", thunderLight = "thunder/light", thunderDark = "thunder/dark"
    case lightLight = "light/light", lightDark = "light/dark"
    case darkDark = "dark/dark"
    // MARK:Rank3
    case fireFireFire = "fire/fire/fire", fireFireIce = "fire/fire/ice", fireFireWind = "fire/fire/wind", fireFireThunder = "fire/fire/thunder", fireFireLight = "fire/fire/light", fireFireDark = "fire/fire/dark"
    case fireIceIce = "fire/ice/ice", fireIceWind = "fire/ice/wind", fireIceThunder = "fire/ice/thunder", fireIceLight = "fire/ice/light", fireIceDark = "fire/ice/dark"
    case fireWindWind = "fire/wind/wind", fireWindThunder = "fire/wind/thunder", fireWindLight = "fire/wind/light", fireWindDark = "fire/wind/dark"
    case fireThunderThunder = "fire/thunder/thunder", fireThunderLight = "fire/thunder/light", fireThunderDark = "fire/thunder/dark"
    case fireLightLight = "fire/light/light", fireLightDark = "fire/light/dark"
    case fireDarkDark = "fire/dark/dark"
    
    case iceIceIce = "ice/ice/ice", iceIceWind = "ice/ice/wind", iceIceThunder = "ice/ice/thunder", iceIceLight = "ice/ice/light", iceIceDark = "ice/ice/dark"
    case iceWindWind = "ice/wind/wind", iceWindThunder = "ice/wind/thunder", iceWindLight = "ice/wind/light", iceWindDark = "ice/wind/dark"
    case iceThunderThunder = "ice/thunder/thunder", iceThunderLight = "ice/thunder/light", iceThunderDark = "ice/thunder/dark"
    case iceLightLight = "ice/light/light", iceLightDark = "ice/light/dark"
    case iceDarkDark = "ice/dark/dark"
    
    case windWindWind = "wind/wind/wind", windWindThunder = "wind/wind/thunder", windWindLight = "wind/wind/light", windWindDark = "wind/wind/dark"
    case windThunderThunder = "wind/thunder/thunder", windThunderLight = "wind/thunder/light", windThunderDark = "wind/thunder/dark"
    case windLightLight = "wind/light/light", windLightDark = "wind/light/dark"
    case windDarkDark = "wind/dark/dark"
    
    case thunderThunderThunder = "thunder/thunder/thunder", thunderThunderLight = "thunder/thunder/light", thunderThunderDark = "thunder/thunder/dark"
    case thunderLightLight = "thunder/light/light", thunderLightDark = "thunder/light/dark"
    case thunderDarkDark = "thunder/dark/dark"
    
    case lightLightLight = "light/light/light", lightLightDark = "light/light/dark"
    case lightDarkDark = "light/dark/dark"
    
    case darkDarkDark = "dark/dark/dark"
    
}
enum AttSortEm: String {
    case none, first, random, highHp, noneStatusAfterFirst, noneStatusAfterRandom
}

import Foundation
var elementModel: ElementModel?

struct ElementModel: Codable {
    var elementType: ElementTypeEm?
    var buffStatus: BuffStatus_ElementModel = BuffStatus_ElementModel()
    var debuffStatus: DebuffStatus_ElementModel = DebuffStatus_ElementModel()
    var elementStatus: ElementStatus_ElementModel = ElementStatus_ElementModel()
    var elementLv: Int = 1
    
    enum CodingKeys: String, CodingKey {
        case buffStatus, debuffStatus, elementStatus, elementLv
        /* 增益狀態, 減益狀態 */
    }
}

struct BuffStatus_ElementModel: Codable {
    var attUp: Bool = false
    var attSpeedUp: Bool = false
    var criticalStrikeChanceUp: Bool = false
    var criticalStrikeDamageUp: Bool = false
    var sacred: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case attUp, attSpeedUp, criticalStrikeChanceUp, criticalStrikeDamageUp, sacred
        /* 攻擊力上升, 攻速上升, 爆擊上升, 爆傷上升, 神聖 */
    }
}
struct DebuffStatus_ElementModel: Codable {
    var attDown: Bool = false
    var attSpeedDown: Bool = false
    var criticalStrikeChanceDown: Bool = false
    var criticalStrikeDamageDown: Bool = false
    var imprison: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case attDown, attSpeedDown, criticalStrikeChanceDown, criticalStrikeDamageDown, imprison
        /* 降攻擊力, 降攻速, 降爆擊, 降爆傷, 封印 */
    }
}

struct ElementStatus_ElementModel: Codable {
    var attSort: AttSortEm?
    var attSortStr: String?
    var att: String?
    var outAtt: String?
    var inAtt: String?
    var attSpeed: String?
    var outAttSpeed: String?
    var inAttSpeed: String?
    
    enum CodingKeys: String, CodingKey {
        case attSortStr, att, outAtt, inAtt, attSpeed, outAttSpeed, inAttSpeed
        /* 攻擊順序 攻擊力 (外)等級加攻 (內)等級加攻 攻擊速度 (外)等級加攻速 (內)等級加攻速 */
    }
}
