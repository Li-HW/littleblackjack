//
//  MonsterModel.swift
//  LittleBlackJack
//
//  Created by 黃楚翔 on 2020/7/8.
//  Copyright © 2020 Jack. All rights reserved.
//

//func <#func name#>(<#Incoming parameters#> completion:@escaping (_ dateResponse: DataResponse<Any>?)->()){
//    let parameters = ["<#Key#>": <#Value#>]
//    NetworkCenter.shard.postApiResponseWithBaseUrl(urlPath: <#Path Name#>, parameters: <#parameters#>) { (dateResponse) in
//        //            print("Api_login statusCode=\(response.response?.statusCode)")
//        guard let _ = dateResponse.response else {
//            //網路斷線時
//            completion(nil)
//            return
//        }
//        completion(dateResponse)
//    }
//}

import Foundation
//let <#Path Name#>: String = "<#Path String#>"
var monsterModel: MonsterModel?

struct MonsterModel: Codable {
    var hp: Int?
    var runSpeed: Int?
    var debuffStatus: DebuffStatus_MonsterModel = DebuffStatus_MonsterModel()
    var immunityStatus: ImmunityStatus_MonsterModel = ImmunityStatus_MonsterModel()
    var skills: Skills_MonsterModel = Skills_MonsterModel()
    
    enum CodingKeys: String, CodingKey {
        case hp, runSpeed, debuffStatus, immunityStatus, skills
        /* 血量, 移動速度, 減益狀態, 免疫狀態, 技能 */
    }
}

struct DebuffStatus_MonsterModel: Codable {
    var burn: Bool = false
    var deathExplosion: Bool = false
    var slow: Bool = false
    var ice: Bool = false
    var floating: Bool = false
    var paralysis: Bool = false
    var curse: Bool = false
    var chaos: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case burn, deathExplosion, slow, ice, floating, paralysis, curse, chaos
        /* 燃燒, 死亡爆炸, 緩速, 冰凍, 漂浮, 麻痹, 詛咒, 混沌 */
    }
}

struct ImmunityStatus_MonsterModel: Codable {
    var burn: Bool = false
    var slow: Bool = false
    var floating: Bool = false
    var paralysis: Bool = false
    var curse: Bool = false
    var chaos: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case burn, slow, floating, paralysis, curse, chaos
        /* 燃燒, 緩速, 漂浮, 麻痹, 詛咒, 混沌 */
    }
}

struct Skills_MonsterModel: Codable {
    var imprisonElement: Bool = false
    var breakElement: Bool = false
    var elementLvDown: Bool = false
    var monsterDebuffClear: Bool = false
    var selfDebuffClear: Bool = false
    var selfSprint: Bool = false
    var monsterSpeedBuff: Bool = false
    var selfHeal: Bool = false
    var areaHeal: Bool = false
    var monsterHeal: Bool = false
    var areaImmunityDebuff: Bool = false
    var selfImmunityControl: Bool = false
    var areaImmunityControl: Bool = false
    var deathSplit: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case imprisonElement, breakElement, elementLvDown, monsterDebuffClear, selfDebuffClear, selfSprint, monsterSpeedBuff, selfHeal, areaHeal, monsterHeal, areaImmunityDebuff, selfImmunityControl, areaImmunityControl, deathSplit
        /* 隨機封印N元素, 隨機破壞N元素, 隨機降低N元素LV, 小怪解除減益狀態, 解除自體減益狀態, 自體衝鋒, 小怪加速, 本體補血, 範圍補血, 小怪補血, 範圍免疫減益, 自體免疫控場, 範圍免疫控場, 死亡分裂 */
    }
}
